labwc (0.8.3-1) unstable; urgency=medium

  * New upstream release

 -- Birger Schacht <birger@debian.org>  Sat, 22 Feb 2025 09:25:03 +0100

labwc (0.8.2-3) unstable; urgency=medium

  * Add libsfdo-dev to build-depends and enable title bar support
  * Run wrap-and-sort -ast on debian/

 -- Birger Schacht <birger@debian.org>  Mon, 10 Feb 2025 15:54:37 +0100

labwc (0.8.2-2) unstable; urgency=medium

  * Add debian/labwc.docs to also ship upstreams NEWS.md file
    (Closes: #1090896)

 -- Birger Schacht <birger@debian.org>  Wed, 01 Jan 2025 19:36:13 +0100

labwc (0.8.2-1) unstable; urgency=medium

  * New upstream release
  * d/control: bump wlroots dependency to >= 0.18.1

 -- Birger Schacht <birger@debian.org>  Tue, 31 Dec 2024 12:33:58 +0100

labwc (0.8.1-2) unstable; urgency=medium

  * Reupload to unstable

 -- Birger Schacht <birger@debian.org>  Thu, 26 Dec 2024 15:16:29 +0100

labwc (0.8.1-1) experimental; urgency=medium

  * New upstream release
  * d/rules: disable icon support

 -- Birger Schacht <birger@debian.org>  Mon, 28 Oct 2024 10:33:44 +0100

labwc (0.8.0-1) experimental; urgency=medium

  * New upstream release

 -- Birger Schacht <birger@debian.org>  Fri, 16 Aug 2024 18:20:46 +0200

labwc (0.7.4-1) unstable; urgency=medium

  * New upstream release

 -- Birger Schacht <birger@debian.org>  Sat, 20 Jul 2024 08:08:14 +0200

labwc (0.7.3-1) unstable; urgency=medium

  * New upstream release

 -- Birger Schacht <birger@debian.org>  Sat, 13 Jul 2024 14:00:43 +0200

labwc (0.7.2-2) unstable; urgency=medium

  * Move terminal from Recommends to Suggests and make it less strict
  * Drop `labwc.install` and not install example configs as defaults

 -- Birger Schacht <birger@debian.org>  Sat, 22 Jun 2024 08:56:37 +0200

labwc (0.7.2-1) unstable; urgency=medium

  * New upstream release

 -- Birger Schacht <birger@debian.org>  Sat, 11 May 2024 08:39:48 +0200

labwc (0.7.1-2) unstable; urgency=medium

  * Set Maintainer to team address

 -- Birger Schacht <birger@debian.org>  Fri, 26 Apr 2024 09:49:11 +0200

labwc (0.7.1-1) unstable; urgency=medium

  * Drop patch to set default terminal to `foot`. Upstream sets the default
    to alacritty, which is also in Debian, lets not diverge
  * Add alacritty to Recommends: so users have a way of doing the initial
    setup
  * Replace pkg-config build dep with pkgconf
  * Update years in d/copyright

 -- Birger Schacht <birger@debian.org>  Sat, 02 Mar 2024 08:29:21 +0100

labwc (0.7.0-3) unstable; urgency=medium

  * Add librsvg to build depends and xwayland to depends

 -- Birger Schacht <birger@debian.org>  Mon, 05 Feb 2024 13:59:54 +0100

labwc (0.7.0-2) unstable; urgency=medium

  * Reupload to unstable

 -- Birger Schacht <birger@debian.org>  Sat, 13 Jan 2024 11:03:20 +0100

labwc (0.7.0-1) experimental; urgency=medium

  * New upstream release (Closes: #1058499)
  * Bump wlroots build depends to >= 0.17
  * Add new build-dep liblzma-dev
  * Cleanup indenting in d/control

 -- Birger Schacht <birger@debian.org>  Sat, 16 Dec 2023 14:18:36 +0100

labwc (0.6.6-1) unstable; urgency=medium

  * New upstream release
  * Update patch: default environment is commented, so no patching needed
    anymore

 -- Birger Schacht <birger@debian.org>  Sun, 26 Nov 2023 09:56:27 +0100

labwc (0.6.5-1) unstable; urgency=medium

  * New upstream release
  * Remove stale file entries from d/copyright

 -- Birger Schacht <birger@debian.org>  Wed, 01 Nov 2023 17:32:27 +0100

labwc (0.6.4-1) unstable; urgency=medium

  * Initial upload (Closes: #1032864)

 -- Birger Schacht <birger@debian.org>  Wed, 30 Aug 2023 21:43:33 +0200
